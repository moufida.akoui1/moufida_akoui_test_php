<?php
require_once "connection.php";

if(isset($_POST['save']))
{    

     $name = $_POST['name'];
     $birthdate=$_POST['birthdate'];
     $adresse=$_POST['adresse'];
     $website=$_POST['website'];
     $email = $_POST['email'];
     $creation_date=date("Y-m-d");
     $sql = "INSERT INTO test (name,birthdate,adresse,website,email,creation_date)
     VALUES ('$name','$birthdate','$adresse','$website','$email','$creation_date')";
     if (mysqli_query($conn, $sql)) {
        header("location: index.php");
        exit();
     } else {
        echo "Error: " . $sql . "
" . mysqli_error($conn);
     }
     mysqli_close($conn);
}
?>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <title>Formulaire d'ajout</title>
    <?php include "head.php"; ?>
</head>
<body>
 
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header">
                        <h2>Rempliser le formulaire </h2>
                    </div>
                   
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group">
                            <label>Nom</label>
                            <input type="text" name="name" class="form-control" value="" maxlength="50" required="">
                        </div>
                        <div class="form-group">
                            <label>Date de Naissance</label>
                            <input type="date" name="birthdate" class="form-control" value="" maxlength="12" required="">
                        </div>
                        <div class="form-group">
                            <label>Adresse</label>
                            <input type="texte" name="adresse" class="form-control" value="" maxlength="12" required="">
                        </div>
                        <div class="form-group">
                            <label>Site internet</label>
                            <input type="texte" name="website" class="form-control" value="" maxlength="12" required="">
                        </div>
                        <div class="form-group ">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" value="" maxlength="30" required="">
                        </div>
                       

                        <input type="submit" class="btn btn-primary" name="save" value="Ajouter">
                        <a href="index.php" class="btn btn-default">Annuler</a>
                    </form>
                </div>

            </div> 
               
        </div>

</body>
</html>
