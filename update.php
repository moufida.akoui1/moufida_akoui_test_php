<?php
// Include database connection file
require_once "connection.php";

    if(count($_POST)>0) {
    mysqli_query($conn,"UPDATE test set  name='" . $_POST['name'] . "', birthdate='" . $_POST['birthdate'] . "' ,adresse='". $_POST['adresse'] . "' ,website='". $_POST['website']."',email='" . $_POST['email'] . "' WHERE id='" . $_POST['id'] . "'");
     
     header("location: index.php");
     exit();
    }
    $result = mysqli_query($conn,"SELECT * FROM test WHERE id='" . $_GET['id'] . "'");
    $row= mysqli_fetch_array($result);
  
?>
 
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <title>Modification</title>
    <?php include "head.php"; ?>
</head>
<body>

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header">
                        <h2>Modifier l'utilisateur</h2>
                    </div>
                  
                    <form action="<?php echo htmlspecialchars(basename($_SERVER['REQUEST_URI'])); ?>" method="post">
                        <div class="form-group">
                            <label>Nom</label>
                            <input type="text" name="name" class="form-control" value="<?php echo $row["name"]; ?>" maxlength="50" required="">
                            
                        </div>
                        <div class="form-group">
                            <label>Date de naissance</label>
                            <input type="date" name="birthdate" class="form-control" value="<?php echo $row["birthdate"]; ?>" maxlength="12"required="">
                        </div>
                         <div class="form-group">
                            <label>Adresse</label>
                            <input type="texte" name="adresse" class="form-control" value="<?php echo $row["adresse"]; ?>" maxlength="12"required="">
                        </div>
                         <div class="form-group">
                            <label>Site Web</label>
                            <input type="texte" name="website" class="form-control" value="<?php echo $row["website"]; ?>" maxlength="12"required="">
                        </div>
                        <div class="form-group ">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" value="<?php echo $row["email"]; ?>" maxlength="30" required="">
                        </div>
                        
                        <input type="hidden" name="id" value="<?php echo $row["id"]; ?>"/>
                        <input type="submit" class="btn btn-primary" value="Modifier">
                        <a href="index.php" class="btn btn-default">Annuler</a>
                    </form>
                </div>
            </div>  
        </div>
</body>
</html>