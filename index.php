<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>Test Moufida Akoui</title>
<?php include "head.php"; ?>

    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>

</head>
<body>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mx-auto">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Accueil</h2>
                        <a href="create.php" class="btn btn-danger pull-right">Ajouter</a>
                    </div>
                    <div>
                       <a href="index.php?action=asctri" class="btn btn-warning pull-right">ASC</a>&nbsp;&nbsp;
                      <a href="index.php" class="btn btn-success pull-right">DESC</a> 
                       <br></br>
                    </div>
                   <?php
                    include_once 'connection.php';
                    if(isset($_GET['action'])){
                    $result = mysqli_query($conn,"SELECT *,DATE_FORMAT(creation_date, 'Le %d %M %Y') AS date FROM test ORDER BY date ASC ");
                    }
                    if(!isset($_GET['action'])){
                    $result = mysqli_query($conn,"SELECT *,DATE_FORMAT(creation_date, 'Le %d %M %Y') AS date FROM test ORDER BY date DESC ");
                    }
                    ?>

                    <?php
                    if (mysqli_num_rows($result) > 0) {
                    ?>
                      <table class='table table-bordered table-striped'>
                      
                      <tr>
                        <td>Nom</td>
                        <td>Date de naissance</td>
                        <td>Adresse</td>
                        <td>Site Web</td>
                        <td>Email</td>
                         <td>Date de création</td>
                         <td>Options</td>
                      </tr>
                    <?php
                    $i=0;
                    while($row = mysqli_fetch_array($result)) {
                    ?>
                    <tr>
                        <td><?php echo $row["name"]; ?></td>
                        <td><?php echo $row["birthdate"]; ?></td>
                        <td><?php echo $row["adresse"]; ?></td>
                        <td><?php echo $row["website"]; ?></td>
                        <td><?php echo $row["email"]; ?></td>
                        <td><?php echo $row["creation_date"]; ?></td>
                        <td><a href="update.php?id=<?php echo $row["id"]; ?>" title='Modifier'><span class='glyphicon glyphicon-pencil'></span></a>
                        <a href="delete.php?id=<?php echo $row["id"]; ?>" title='Supprimer'><i class='material-icons'><span class='glyphicon glyphicon-trash'></span></a>
                        </td>
                    </tr>
                    <?php
                    $i++;
                    }
                    ?>
                    </table>
                     <?php
                    }
                    else{
                        echo "pas de résultat ";
                    }
                    ?>

                </div>
            </div>     
        </div>

</body>
</html>