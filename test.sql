-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  Dim 02 fév. 2020 à 21:09
-- Version du serveur :  10.2.31-MariaDB
-- Version de PHP :  7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `netmixco_wp979`
--

-- --------------------------------------------------------

--
-- Structure de la table `test`
--

CREATE TABLE `test` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `birthdate` date NOT NULL,
  `adresse` text NOT NULL,
  `website` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `creation_date` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `test`
--

INSERT INTO `test` (`id`, `name`, `birthdate`, `adresse`, `website`, `email`, `creation_date`) VALUES
(16, 'moufida akoui', '2020-02-04', 'djerba midon', 'moufida.tn', 'moufida@jerba.com', '2020-02-02'),
(15, 'donald', '2020-02-29', 'america', 'trump.com', 'trump@tump.trump', '2020-02-02'),
(17, 'krmldgdw', '2020-02-22', 'klsdglsÃ¹', 'dsdg,lsdÃ¹', 'moff@gmail.com', '2020-02-02'),
(13, 'ahmed', '2020-02-06', 'tunis', 'ahmed.vom', 'ahmed.hlaimi1@gmail.com', '2020-01-01');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `test`
--
ALTER TABLE `test`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
